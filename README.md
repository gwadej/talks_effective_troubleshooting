# Effective Troubleshooting

This presentation is intended to be given to a group containing unknown or
mixed skill with troubleshooting. The goal is to focus the audience's
understanding of troubleshooting to result in a more effective approach.

The original talk was intended for other developers. I've since customized
it (slightly) for non-developers (support, QA, etc.).

* [For developers](effective_troubleshooting-dev.html)
* [For non-developers](effective_troubleshooting.html)
